﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScreenAudio : MonoBehaviour
{
    public AudioSource audioSourceIntro;
    public AudioSource audioSourceLoop;
    public AudioSource audioSourceBack;
    private bool startedLoop;

    void FixedUpdate()
    {
        if (audioSourceBack.isPlaying)
        {
            audioSourceBack.Stop();
        }
        if (!audioSourceIntro.isPlaying && !startedLoop)
        {
            audioSourceLoop.Play();
            Debug.Log("Done playing");
            startedLoop = true;
        }
    }
}
