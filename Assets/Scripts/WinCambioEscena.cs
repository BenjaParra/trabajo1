﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinCambioEscena : MonoBehaviour
{
    public void CambioEscena()
    {
        SceneManager.LoadScene(0);           
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            CambioEscena();
        }
    }
}
