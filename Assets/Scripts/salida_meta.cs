﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class salida_meta : MonoBehaviour
{
    public bool llego;
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.name == "salida" || hit.collider.tag == "colisionador" && !llego)
        {
            Debug.Log("llegue a la meta");
            llego = true;
            SceneManager.LoadScene(2);
        }

       
    }
}
