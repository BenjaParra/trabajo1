﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CambiarEscena : MonoBehaviour
{
    public GameObject DifficultyToggles;

    private void Start()
    {
        DifficultyToggles.transform.GetChild((int)GameValuess.Difficulty).GetComponent<Toggle>().isOn = true;
    }

    public void CambiodeEscena()
    {
        SceneManager.LoadScene(1);
    }

    #region Difficulty
    public void SetEasyDifficulty(bool isOn)
    {
        if (isOn)
            GameValuess.Difficulty = GameValuess.Diffculties.Easy;
    }
    public void SetMediumDifficulty(bool isOn)
    {
        if (isOn)
            GameValuess.Difficulty = GameValuess.Diffculties.Medium;
    }
    public void SetHardDifficulty(bool isOn)
    {
        if (isOn)
            GameValuess.Difficulty = GameValuess.Diffculties.Hard;
    }
    #endregion
}
