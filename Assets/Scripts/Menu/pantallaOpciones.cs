﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pantallaOpciones : MonoBehaviour
{
    public ControladorOpciones panelOpciones;

    private void Start()
    {
        panelOpciones = GameObject.FindGameObjectWithTag("Opciones").GetComponent<ControladorOpciones>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            MostrarOpciones();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            OcultarOpciones();
        }
    }

    public void MostrarOpciones()
    {
        panelOpciones.pantallaOpciones.SetActive(true);
    }
    
    public void OcultarOpciones()
    {
        panelOpciones.pantallaOpciones.SetActive(false);
    }
}
