﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EntreEscenas : MonoBehaviour
{
    public AudioSource AudioBack;
   
    private void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;
        if (AudioBack.isPlaying && sceneName == "EndgameWin" || sceneName == "EndgameLose")
        {
            AudioBack.Stop();
        }
    }
    private void Awake()
    {
        

        //var noDestruirEntreEscenas = FindObjectsOfType<EntreEscenas>();
        //if(noDestruirEntreEscenas.Length > 1)
        //{
        //    Destroy(gameObject);
        //    return;
        //}

        DontDestroyOnLoad(gameObject);

        
    }
}
