﻿
using UnityEngine;

public class Target : MonoBehaviour
{
    public float health;
    public void TakeDamage(float amount)
    {
        health -= amount;
        if(health <= 0f)
        {
            Die();
        }

        void Die()
        {
            Destroy(gameObject);
        }
    }

    


    private void Start()
    {
        switch (GameValuess.Difficulty)
        {
            case (GameValuess.Diffculties.Easy):
                health = 20;
                break;
            case (GameValuess.Diffculties.Medium):
                health = 30;
                break;
            case (GameValuess.Diffculties.Hard):
                health = 50;
                break;
        }
    }
}
